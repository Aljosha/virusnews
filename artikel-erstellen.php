<!DOCTYPE html>
<html lang="en">
  <head>
  <?php
    session_start();
    if(!isset($_SESSION['userid'])) {
     die('<meta http-equiv="refresh" content="0; URL=http://test.aj-v.de/virusnewscp/login.php">');
    }
  ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script> 
  <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 
  <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
  <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>
    <title>Artikel erstellen | Virus</title>

    <link href="res/mainstyle.css" rel="stylesheet">

    <?php
     include("res/css.php");
     ?>

  </head>

  <body>


    <!-- Fixed navbar -->
    <?php
     include("navbar.php");
     ?>
    

    <!-- Begin page content -->
    <div class="container">
      <div class="page-header">
        <h1>Artikel erstellen:</h1>
      </div>
      <?php
        $text = $_POST['text'];
        $title = $_POST['title'];
        $category = $_POST['category'];
        $author = $_POST['author'];
        $link = $_POST['link'];

        $submitstatus = $_GET['submit'];

        if($submitstatus == 1){
          if($text != null && $title != null && $category != null && $author != null && $link!= null){

            $pdo = new PDO('mysql:host=localhost;dbname=virusnews', '', '');
 
            $statement = $pdo->prepare("INSERT INTO content (author, title, category, text, link) VALUES (?, ?, ?, ?, ?)");
            $statement->execute(array($author, $title, $category, $text, $link));  



            echo '<div class="alert alert-success" role="alert">
                  <strong>Glückwunsch!</strong> Der Artikel wurde erfolgreich <a href="http://test.aj-v.de/virusnewscp/artikel.php?id='.$link.'"" class="alert-link">hier</a> veröffentlicht.
                  </div>';

          } else {
            echo '<div class="alert alert-danger" role="alert">
            <strong>Das hat nicht geklappt! </strong> Fülle alle Felder aus und versuche es nochmal.
            </div>';
          }

        }

      ?>

      <div class="row">
                <form class="span12" id="postForm" action="?submit=1" method="POST" enctype="multipart/form-data" >

                    <fieldset>
                        <label for="summernote">Artikel</label>
                        <p class="container">
                            <textarea class="input-block-level" id="summernote" name="text" rows="18">
                            </textarea>
                        </p>
                    </fieldset>

                    <fieldset class="form-group">
                      <label for="title">Titel</label>
                      <input class="form-control" name="title" id="title" placeholder="Titel">
                    </fieldset>

                    <fieldset class="form-group">
                      <label for="category">Kategorie</label>
                      <input class="form-control" name="category" id="category" placeholder="Kategorie">
                    </fieldset>

                    <fieldset class="form-group">
                      <label for="author">Autor</label>
                      <input class="form-control" name="author" id="author" placeholder="Autor">
                    </fieldset>

                    <fieldset class="form-group">
                      <label for="link">Link</label>
                      <input class="form-control" name="link" id="link" placeholder="Letzter Teil des Links">
                    </fieldset>

                    
                    <button type="submit" class="btn btn-primary">Speichern</button>
                </form>
            </div>

  <script>
    $(document).ready(function() {
        $('#summernote').summernote({
  toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']]
  ]
  , disableDragAndDrop: true
  , height: 300
});

    });
  </script>

  </body>
</html>
