<!DOCTYPE html>
<html lang="en">
  <head>
  <?php
    session_start();
    if(!isset($_SESSION['userid'])) {
     die('<meta http-equiv="refresh" content="0; URL=http://virus.aj-v.de/login.php">');
    }
  ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>VIRUS CP</title>

    <link href="res/mainstyle.css" rel="stylesheet">

    <?php
     include("res/css.php");
     ?>

  </head>

  <body>

    <?php
     include("navbar.php");
     ?>
    

    <div class="container">
      <div class="page-header">
        <h1>Virus | Administration</h1>
      </div>

      <p class="lead">Willkommen im Administrationsbereich der Virus-Zeitung.</p>
      <p>Hier kannst du neue Artikel veröffentlichen.</p>
    </div>

    <footer class="footer">
      <div class="container">
        <p class="text-muted">2016 by Aljosha</p>
      </div>
    </footer>

    <?php
  include("res/js.php");
  ?>
  </body>
</html>
