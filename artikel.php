<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php
     $link = $_GET['id'];
     $pdo = new PDO('mysql:host=localhost;dbname=', '', '');

     $sql = "SELECT * FROM content WHERE link = '".$link."'"; 
     $content = $pdo->query($sql)->fetch();
           $text = $content['text'];
           $title = $content['title'];
           $category = $content['category'];
           $author = $content['author'];
    ?>


    <title><?php echo $title; ?></title>

    <link href="res/articlestyle.css" rel="stylesheet">

    <?php
     include("res/css.php");
     ?>
     <style>

      img{display: inline; height: auto; max-width: 100%;}

    </style>
  </head>

  <body>



    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="inner cover">
            <h1 class="cover-heading"><?php echo $title; ?></h1>
            <article>
            <p class="lead"><?php echo $text; ?></p>
            </article>
            <author>
            <p class="lead">geschrieben von <?php echo $author; ?></p>
            </author>
          </div>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <?php
  include("res/js.php");
  ?>
  </body>
</html>
