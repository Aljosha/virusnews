<?php 
session_start();
$pdo = new PDO('mysql:host=localhost;dbname=', '', '');
 
if(isset($_GET['login'])) {
  $email = $_POST['email'];
  $passwort = $_POST['passwort'];
  
  $statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
  $result = $statement->execute(array('email' => $email));
  $user = $statement->fetch();
    
  //Überprüfung des Passworts
  if ($user !== false && password_verify($passwort, $user['passwort'])) {
    $_SESSION['userid'] = $user['id'];
    die('<meta http-equiv="refresh" content="0; URL=http://test.aj-v.de/virusnewscp/">');
  } else {
    $errorMessage = "E-Mail oder Passwort war ungültig<br>";
  }
  
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Login | Virus</title>

	<link href="res/style.css" rel="stylesheet">

	<?php
	include("res/css.php");
	?>

</head>
<body>

	 <div class="container">

      <form action="?login=1" method="post" class="form-signin">
        <h2 class="form-signin-heading">Anmelden</h2>
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email" required autofocus>
        <label for="inputPassword" class="sr-only">Passwort</label>
        <input type="password" id="inputPassword" class="form-control" name="passwort" placeholder="Passwort" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Anmeldung speichern
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Anmelden</button>
      </form>

    </div>


	<?php
	include("res/js.php");
	?>

</body>
</html>